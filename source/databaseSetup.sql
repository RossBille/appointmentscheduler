DROP TABLE IF EXISTS AppointmentScheduler.AppointmentHistory;
DROP TABLE IF EXISTS AppointmentScheduler.Appointment;
DROP TABLE IF EXISTS AppointmentScheduler.User;


CREATE TABLE AppointmentScheduler.User
(
    UserID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Username      VARCHAR(20)   NOT NULL  UNIQUE,
    Firstname     CHAR(15)      NOT NULL,
    Lastname      CHAR(15)      NOT NULL,
    Email         VARCHAR(150)  NOT NULL
);

CREATE TABLE AppointmentScheduler.Appointment
(
    AppointmentID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Time DATETIME NOT NULL,
    Duration INT(10),
    Subject VARCHAR(25),
    Message VARCHAR(2000)
);

CREATE TABLE AppointmentScheduler.AppointmentHistory
(
	UserID INT(10) NOT NULL,
    AppointmentID INT(10) NOT NULL,
    Approved BOOLEAN NOT NULL DEFAULT false,
    Required BOOLEAN NOT NULL DEFAULT false,
	PRIMARY KEY (UserID, AppointmentID),
    FOREIGN KEY (UserID) REFERENCES AppointmentScheduler.User(UserID),
    FOREIGN KEY (AppointmentID) REFERENCES AppointmentScheduler.Appointment(AppointmentID)
	
);

-- insertion
INSERT INTO User VALUES
(0,'rossjb', 'Ross', 'Bille', 'ross.j.bille@newcastle.edu.au'),
(0, 'yuqingl', 'Yuqing', 'Lin', 'yuqing.lin@newcastle.edu.au'),
(0, 'danielk', 'Daniel', 'Kopko', 'daniel.kopko@uon.edu.au');
