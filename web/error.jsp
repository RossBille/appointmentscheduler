<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>500 Error</title>
    </head>
    <body>
        <h1>Error!</h1>
        <div>
            There was an error with the request.<br />
            Please Click 
            <a href="
            <%-- if there is no remote user, direct to the logout servlet to invalidate the session and ask user to login --%>
            <%if(request.getRemoteUser().isEmpty())
            {%>
                Logout
            <%}else{%> <%-- else re-run the setup servlet --%>
                Init?refresh=true
            <%}%><%-- end if --%>
            ">here</a> to recover from this error
    </body>
</html>
