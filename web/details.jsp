<%@page import="java.util.LinkedList"%>
<%@page import="database.User"%>
<%@page import="database.AppointmentDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="userInterface.Bean" id="bean" scope="session" />
<!DOCTYPE html>
<html>
    <head>
        <!-- include style sheets -->
        <link rel="stylesheet" type="text/css" href="css/details.css">
        <!-- include jQuery library -->
        <script type="text/javascript" src="js/jQuery.js"></script>
        <!-- include javascript files -->
        <script type="text/javascript" src="js/details.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Appointment Details</title>
    </head>
    <body style="overflow-y: scroll;">
        <div class="container">
            <%-- the main appointment is the appointment that the user selected on the previous page--%>
            <div class="appointment" id="main">
                <%AppointmentDetails ad = (AppointmentDetails) session.getAttribute("appointment");%>
                <strong>Subject:</strong> <%=ad.getAppointment().getSubject()%><br />
                <strong>Date:</strong> <%=ad.getDate()%><br />
                <strong>Time:</strong> <%=ad.getTime()%> - <%=ad.getEndTime()%><br />
                <div class="users">
                    <div class="float">
                        <strong>Required:</strong>
                        <div class="required">
                            <table>
                                <%for(User u : ad.getRequiredUsers()){%>
                                    <tr>
                                        <td>
                                            <%=u.getFirstName()%> <%=u.getLastName()%>
                                        </td>
                                    </tr>
                                <%}%>
                            </table>
                        </div><!-- end required -->
                    </div><!-- end float -->
                    <div class="float">
                        <strong>Guests:</strong>
                        <div class="guests">                            
                            <table>
                                <%for(User u : ad.getOptionalUsers()){%>
                                    <tr>
                                        <td>
                                            <%=u.getFirstName()%> <%=u.getLastName()%>
                                        </td>
                                    </tr>
                                <%}%>
                            </table>
                        </div><!-- end guests -->
                    </div><!-- end float -->
                </div><!-- end users -->
                <div class="message">
                    <strong>Message:</strong>
                    <div class="contents">
                        <%=ad.getAppointment().getMessage()%>
                    </div><!-- end contents -->
                </div>
                <div class="action">
                    <strong>Action:</strong><br />
                    <%if(ad.isApproved() || ad.isAttending()){%>
                        You are Attending this meeting.
                        <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=ad.getAppointment().getAppointmentId()%>&accept=false';" value="Cancel" />
                    <%}else{%>
                        <%if(ad.isRequired()){%>
                            <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=ad.getAppointment().getAppointmentId()%>&accept=true';" value="Accept" />
                            <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=ad.getAppointment().getAppointmentId()%>&accept=false';" value="Reject" />
                        <%}else{%>
                            Your attendance is not mandatory.
                        <%}%><%--end if (required)--%>
                    <%}%><%-- end if (approved or attending)--%>
                </div>
            </div>
            <%--
                If there are appointments which conflict with the main appointment
                Iterate through them and display them below
            --%>
            <%LinkedList<AppointmentDetails> overlap = (LinkedList<AppointmentDetails>) session.getAttribute("overlap");%>   
            <%if(overlap.size() > 0){%>
                <span class="warning">The following appointments are conflicting with the above selected appointment:</span>
                <%for(AppointmentDetails appointmentDetails : overlap){%>
                    <div class="appointment conflict maximize">
                        <strong>Subject:</strong> <%=appointmentDetails.getAppointment().getSubject()%><span class="expand"></span><br />
                        <strong>Date:</strong> <%=appointmentDetails.getDate()%><br />
                        <strong>Time:</strong> <%=appointmentDetails.getTime()%> - <%=appointmentDetails.getEndTime()%><br />
                        <div class="users">
                            <div class="float">
                                <strong>Required:</strong>
                                <div class="required">
                                    <table>
                                        <%for(User u : appointmentDetails.getRequiredUsers()){%>
                                            <tr>
                                                <td><%=u.getFirstName()%> <%=u.getLastName()%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                </div>
                            </div>
                            <div class="float">
                                <strong>Guests:</strong>
                                <div class="guests">
                                    <table>
                                        <%for(User u : appointmentDetails.getOptionalUsers()){%>
                                            <tr>
                                                <td><%=u.getFirstName()%> <%=u.getLastName()%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                </div><!-- end guests -->
                            </div><!-- end float -->
                        </div><!-- end users -->
                        <div class="message">
                            <strong>Message:</strong>
                            <div class="contents">
                                <%=appointmentDetails.getAppointment().getMessage()%>
                            </div>
                        </div><!-- end message -->
                        <div class="action">
                            <strong>Action:</strong>
                            <%-- check the current status of the appointment (is the user attending, not required or pending action --%>
                            <%if(appointmentDetails.isApproved() || appointmentDetails.isAttending()){%>
                                You are Attending this meeting.<br />
                                <%--put these back in if you want to add the option to acc/rej conflicted appointments
                                <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=appointmentDetails.getAppointment().getAppointmentId()%>&accept=false';" value="Cancel" />
                                --%>
                            <%}else if(!appointmentDetails.isRequired()){%>
                                Your attendance isn't mandatory.
                            <%}else{%>
                                Pending.<br />
                                <%--put these back in if you want to add the option to acc/rej conflicted appointments
                                <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=appointmentDetails.getAppointment().getAppointmentId()%>&accept=true';" value="Accept" />
                                <input type="button" onclick="parent.location='AcceptMeeting?appointment=<%=appointmentDetails.getAppointment().getAppointmentId()%>&accept=false';" value="Reject" />
                                --%>
                            <%}%><%-- end if --%>
                        </div><!-- end action -->
                    </div><!-- end conflicts -->
                <%}%><%-- end for --%>
            <%}%><%-- end if --%>
        </div><!-- end container -->
    </body>
</html>