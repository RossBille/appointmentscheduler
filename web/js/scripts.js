function validate()
{
    var to = document.getElementById("to");
    var date = document.getElementById("date");
    var duration = document.getElementById("duration");
    var subject = document.getElementById("subject");
    var resultStatus = true;
    var message = "The following errors were detected:";
    if(!to)
    {
        resultStatus = false;
        message += "\n- Could not find any recipients";
    }
    if(!date)
    {
        resultStatus = false;
        message += "\n- Could not find a date";
    }
    if(!duration || duration.value == "")
    {
        resultStatus = false;
        message += "\n- Could not find  duration";
    }
    var toValue = to.value;
    var dateValue = date.value;
    var durationValue = duration.value;
    var meeting = document.getElementById("meeting").value;
    if(toValue == "" && meeting != "")
    {
        resultStatus = false;
        message += "\n- Could not find any recipients";
    }
    if(!dateValue.match(/^\d{4}-\d{1,2}-\d{1,2}$/))
    {
        resultStatus = false;
        message += "\n- Please enter date in yyyy-mm-dd";
    }
    if(isNaN (durationValue-0) || durationValue == null)
    {
        resultStatus = false;
        message += "\n- Please enter a number in duration";
    }
    if(subject.value.length>=25)
    {
        resultStatus = false;
        message += "\n- Subject is too long";
    }
    //add in html line breaks
    var desc = document.getElementById("description").value;
    var newDesc = desc.replace(/(\r\n|\n|\r)/gm, '<br />');
    document.getElementById("description").value = newDesc;
    //check if message is too long after adding in line breaks
    if(newDesc.length >= 2000)
    {
        resultStatus = false;
        message += "\n- Message is too long";
    }
    
    //alert any messages
    if(!resultStatus)
    {
        //remove the recently aded "<br />"
        document.getElementById("description").value = desc;
        //alert of errors
        alert(message);
    }
    
    return resultStatus;
}

