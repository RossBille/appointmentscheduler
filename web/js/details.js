/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function()
{
    var $expand = $('.expand');
    //write a plus symbol in the expand <span> of each conflicting appointment
    $expand.html("+");
    var $conflict = $('.conflict');
    //minimize the appointments by default
    $conflict.children('div').hide();
    $conflict.toggleClass('hidden');
    $conflict.removeClass('maximize');
    //expand/minimize the clicked appointment
    $conflict.click(function(){
        $(this).toggleClass('hidden');
        $(this).children('div').toggle();
        //swap between + and - on each click
        if($(this).children('.expand').html() == "+")
            $(this).children('.expand').html("-");
        else
            $(this).children('.expand').html("+"); 
            
    })
});


