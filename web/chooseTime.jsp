<%@page import="userInterface.Month"%>
<jsp:useBean class="userInterface.Bean" id="bean" scope="session" />
<jsp:useBean class="userInterface.MeetingParams" id="params" scope="session" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- include style sheets -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Possible times</title>
    </head>
    <body>
        <div id="container">
            <h2>Please chose a start time:</h2>
            <%--call the possibleTimes() method to build a table of options for the user--%>
            <%=Month.possibleTimes(params, bean.getUserID())%>
        </div><!-- end container -->
    </body>
</html>