<%@page import="userInterface.Month"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="userInterface.Bean" id="bean" scope="session" />
<%
//if the page needs to be reloaded for a fresh copy
if(bean.isReload())
    //trigger init servlet
    response.sendRedirect("Init?refresh=true");
else 
    //set the variable so next refresh will trigger the Init servlet
    bean.setReload(true);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- include style sheets -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <!-- include jQuery library -->
        <script type="text/javascript" src="js/jQuery.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                //size the window as needed
                $.fn.resize();
            });
            $(window).resize(function()
            {
                //size the window as needed
                $.fn.resize();
            });

            $.fn.resize = function()
            {
                //get the windows width
                var winW = $(window).width();
                var $container = $('#container');
                var $td = $('#weekly_schedule td.blank');
                if(winW < 800)
                {
                    $container.addClass('shrink');
                    $container.css('width', 600);
                    $td.css('width',50);
                }else{
                    $container.removeClass('shrink');
                    $container.css('width', 800);
                    $td.css('width',80);
                }
            }
        </script>
        <title>Home Page </title>
    </head>
    <body>
        <div id="container">
            <div id="left">
                <div class="month">                        
                    <%=Month.buildMonth(bean.getSelected(),0,bean.getAppointments())%>
                </div><!-- end month -->
                <div class="month"> 
                    <%=Month.buildMonth(bean.getSelected(),1,bean.getAppointments())%>
                </div><!-- end month -->
                <div class="month">    
                    <%=Month.buildMonth(bean.getSelected(),2,bean.getAppointments())%>
                </div><!-- end month -->
                <div class="navigation">
                    <input type="image" src="images/prev.jpg" alt="previous" onclick="parent.location='Init?prev=true';" class="nav"/>
                    <input type="image" src="images/next.jpg" alt="next" onclick="parent.location='Init?next=true';" class="nav"/><br />
                    <input class="button" type="button" onclick="parent.location='Init?reset=true';" value="Today" id="today"/><br />
                    <input class="button" type="button" onclick="parent.location='PrepareForm?type=meeting';" value="Create Meeting" id="create"/><br />
                    <input class="button" type="button" onclick="parent.location='createMeeting.jsp?type=block';" value="Block Time-slot" id="block" />
                </div><!-- end navigation -->
            </div><!-- end left -->
            <div id="right">
                <div id="head">
                    <div id="date">
                        <jsp:getProperty name="bean" property="currentDate" />
                    </div><!-- end date -->
                    <div id="login">
                        <jsp:getProperty name="bean" property="user"/><br />
                        <input type="button"  onclick="parent.location='logout';" value="Log Out" />
                        <%--<input type="button"  onclick="parent.location='Init?refresh=true'" value="Refresh" />--%>
                    </div><!-- end login -->
                </div><!-- end head -->
                <div id="weekly_schedule">
                    <%=Month.buildWeek(bean.getAppointments(), bean.getSelected())%> 
                </div><!-- end weekly schedule -->
            </div><!-- end right -->
            <div class="clear"></div>
        </div><!-- end container -->   
    </body>
</html>