<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="userInterface.Bean" id="bean" scope="session" /><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Meeting</title>
        <style type="text/css">
            label 
            { 
                display: inline-block; 
                width: 60px; 
            }
            #container
            {
                margin-left:auto;
                margin-right:auto;
                width:600px;
            }
            #counter
            {
                float: right;
                color: grey;
            }
        </style>
        <script type="text/javascript" src="js/scripts.js"></script>
        <!-- include jQuery library-->
        <script type="text/javascript" src="/AppointmentScheduler/js/jQuery.js"></script>
        <script type="text/javascript">
            //use jQuery to include a message length counter
            $(document).ready(function(){
                var max = 1500;//max chars for the text field
                //run the following every time a key is released while the description box has focus
                $('#description').keyup(function(){
                    if($(this).val().length >= max)
                        $(this).val($(this).val().substr(0, max));
                    else
                        $("#count").text(max-$(this).val().length);                    
                });
            });
        </script>
    </head>
    <body>
        <div id="container">
            <form name="meeting" action="CreateMeeting" onsubmit="return validate();" method="POST">
            <%--Check if the user is trying to create a meeting--%>
            <%if(request.getParameter("type").equals("meeting"))
            {%>
                <h1>Create A Meeting</h1>
                <%--pull the list of users from the request object--%>
                <%LinkedList<String> users = (LinkedList<String>)request.getAttribute("users");%>
                <table>
                    <tr>
                        <th>To:</th>
                        <td>
                            <select multiple="multiple" id="to" name="to">
                                <%
                                //build the select box of users
                                for(String s : bean.getUsers())
                                {%>
                                <option value="<%=s%>"><%=s%></option>
                                <%}%>
                            </select>
                        </td>
                        <th>Cc:</th>
                        <td>
                            <select multiple="multiple" id="cc" name="cc">
                                <%
                                //build the select box of users
                                for(String s : bean.getUsers())
                                {%>
                                <option value="<%=s%>"><%=s%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                </table>
                <label for="subject">Subject:</label>
                <input id="subject" type="text" name="subject" size="87" placeholder="Subject"/><br />
                <label for="description">Message:</label><span id="counter">Characters remaining: <span id="count">1500</span></span><br />
                <textarea id="description"  name="description" cols="71" rows="5"></textarea><br />
                <input id="meeting" name="meeting" type="hidden" value="meeting" />
                
            <%}else{%>
            <%--If the user is trying to block the timeslot--%>
                <h1>Block A Time-slot</h1>
                <input id="meeting" name="meeting" type="hidden" value="" />
                <input id="to" name="to" type="hidden" value="" />
                <input id="cc" name="cc" type="hidden" value="" />
                <input id="subject" type="hidden" value="Blocked" />
                <input id="description" type="hidden" value="Timeslot has been blocked" />
            <%}%> <!-- end if -->
            <%--regardless of the meeting time, the following needs to be present--%>
            <div class="time">
                    Date Range:<br />
                    <label for="date">Date:</label> 
                    <input id="date" type="date" name="date" /><br />
                    <label for="duration">Duration:</label> 
                    <select id="duration" name="duration">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="12">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">All-day</option>
                    </select><br />
                    <input type="radio" name="week_or_day" value="week" checked="checked">Week
                    <input type="radio" name="week_or_day" value="day">Single Day<br />
                </div>  
                <input type="submit" value="Continue">
            </form>
        </div><!-- end container -->
    </body>
</html>