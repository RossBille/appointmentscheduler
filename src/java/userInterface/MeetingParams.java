package userInterface;

import java.util.LinkedList;

/**
 * Simple bean used to hold all the meeting parameters while a meeting is being
 * created
 * <p/>
 * @author Ross Bille 3127333
 */
public class MeetingParams implements java.io.Serializable
{

    private LinkedList<Integer> to;
    private LinkedList<Integer> optional;
    private String subject;
    private String description;
    private boolean isDateRange;
    private long date;
    private int duration;
    private boolean error;

    public MeetingParams()
    {
        this.error = true;
    }

    public MeetingParams(LinkedList<Integer> to, LinkedList<Integer> optional, String subject, String description, boolean isDateRange, long date, int duration)
    {
        this.to = to;
        this.optional = optional;
        this.subject = subject;
        this.description = description;
        this.isDateRange = isDateRange;
        this.date = date;
        this.duration = duration;
        this.error = false;
    }

    public boolean isError()
    {
        return error;
    }

    public void setError(boolean error)
    {
        this.error = error;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date)
    {
        this.date = date;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public boolean isIsDateRange()
    {
        return isDateRange;
    }

    public void setIsDateRange(boolean isDateRange)
    {
        this.isDateRange = isDateRange;
    }

    public LinkedList<Integer> getOptional()
    {
        return optional;
    }

    public void setOptional(LinkedList<Integer> optional)
    {
        this.optional = optional;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public LinkedList<Integer> getTo()
    {
        return to;
    }

    public void setTo(LinkedList<Integer> to)
    {
        this.to = to;
    }

    @Override
    public String toString()
    {
        return "MeetingParams{"
                + "to=" + to
                + ", optional=" + optional
                + ", subject=" + subject
                + ", description=" + description
                + ", isDateRange=" + isDateRange
                + ", date=" + date
                + ", duration=" + duration
                + ", error=" + error + '}';
    }
}
