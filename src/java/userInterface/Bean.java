/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import database.Appointment;
import database.User;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

/**
 *
 * @author Ross Bille 3127333
 */
public class Bean implements java.io.Serializable
{

    private Date selectedDate;
    private int userID;
    private LinkedList<Appointment> appointments;
    private Calendar today;
    private int offset;
    private long selected;
    private String currentDate;
    private User user;
    private LinkedList<String> users;
    private boolean reload;//variable used to decide whether the page needs to be reloaded

    public Bean()
    {
        reload = false;
        today = Calendar.getInstance(Locale.US);
        userID = -1;
        selectedDate = today.getTime();
        offset = today.get(Calendar.MONTH);
        appointments = null;
        selected = today.getTimeInMillis();
        currentDate = new SimpleDateFormat("dd MMMM yyyy").format(today.getTime());
        user = null;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public String getCurrentDate()
    {
        currentDate = new SimpleDateFormat("dd MMMM yyyy").format(today.getTime());
        return currentDate;
    }

    public long getSelected()
    {
        return selected;
    }

    public void setSelected(long selected)
    {
        this.selected = selected;
        today.setTimeInMillis(selected);
    }

    public LinkedList<Appointment> getAppointments()
    {
        return appointments;
    }

    public void setAppointments(LinkedList<Appointment> appointments)
    {
        this.appointments = appointments;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Date getSelectedDate()
    {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate)
    {
        this.selectedDate = selectedDate;
    }

    public Calendar getToday()
    {
        return today;
    }

    public void setToday(Calendar today)
    {
        this.today = today;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public LinkedList<String> getUsers()
    {
        return users;
    }

    public void setUsers(LinkedList<String> users)
    {
        this.users = users;
    }

    public boolean isReload()
    {
        return reload;
    }

    public void setReload(boolean reload)
    {
        this.reload = reload;
    }
    
}
