package userInterface;

import database.Appointment;
import database.DatabaseController;
import database.exceptions.NoSuchEntityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;

/**
 * basically just a helper class to take a lot of the logic out of the JSP's
 * <p/>
 * @author Ross Bille 3127333
 */
public class Month
{

    private static final int BEGINNING = 99;//signify the start of a meeting
    private static final int END = Integer.MAX_VALUE;//signify the end of a meeting
    private static final int BLOCKED = -1;//signify a blocked timeslot
    private static final int MIDDLE = 1;
    private static final int SINGLE = Integer.MIN_VALUE;

    /**
     * fully structured html table representing a calendar of the of the month
     * that is "offset" months away from "selected" each date (except for the
     * selected date) are clickable to select a new date Dates with >0
     * appointments in them have css class="activity" the row containing the
     * selected date has css class="selected" the selected date also has the css
     * class="selected'
     * <p/>
     * @param selected the date of the active month in miliseconds
     * @param monthOffset how many months away from the selected month is to be
     * printed
     * @param appointments the list of appointments the current user is involved
     * in
     * @return string representation of the table
     */
    public static String buildMonth(long selected, int monthOffset, LinkedList<Appointment> appointments)
    {
        String link = "Init";
        int baseTab = 7;
        //build calendar
        Calendar now = Calendar.getInstance(Locale.US);
        //set to the correct date
        now.setTimeInMillis(selected);
        now.add(Calendar.MONTH, monthOffset);
        //clear time values
        now.set(Calendar.MILLISECOND, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);

        //find which cell to start the calendar on
        now.set(Calendar.DAY_OF_MONTH, 1);
        int current = now.get(Calendar.DAY_OF_WEEK) * -1;
        //set flags
        Calendar selectedCalendar = Calendar.getInstance(Locale.US);
        selectedCalendar.setTimeInMillis(selected);
        int selectedMonth = selectedCalendar.get(Calendar.MONTH);
        int selectedDay = selectedCalendar.get(Calendar.DAY_OF_MONTH);
        int thisMonth = now.get(Calendar.MONTH);

        //set limits
        int days_in_month = now.getActualMaximum(Calendar.DAY_OF_MONTH);
        int weeks_in_month = now.getActualMaximum(Calendar.WEEK_OF_MONTH);
        //build table
        String monthName = new SimpleDateFormat("MMMM - yyyy").format(now.getTimeInMillis());
        String str = monthName + "\n" + tab(baseTab) + "<table>\n";
        str += tab(baseTab + 1) + "<tr>\n"
                + tab(baseTab + 2) + "<th>S</th><th>M</th><th>T</th><th>W</th><th>T</th><th>F</th><th>S</th>\n"
                + tab(baseTab + 1) + "</tr>\n";

        for(int i = 1; i <= weeks_in_month; i++)
        {
            String row = tab(baseTab + 2) + "<tr>\n";
            String td = "";
            for(int j = 1; j <= 7; j++)
            {
                current++;
                if(current == 0)
                    current = 1;
                now.set(Calendar.DAY_OF_MONTH, current);
                if(current <= 0 || current > days_in_month)//date is out of range
                {
                    td += tab(baseTab + 2) + "<td>&nbsp;</td>\n";
                }else
                {
                    if(thisMonth == selectedMonth && current == selectedDay)
                    {
                        boolean activity = false;
                        for(Appointment a :appointments)
                        {
                            //if there is any activity on this day, bold the link
                            Calendar tempCalendar = Calendar.getInstance(Locale.US);
                            tempCalendar.setTime(a.getTime());

                            if(tempCalendar.get(Calendar.YEAR) == now.get(Calendar.YEAR)
                                    && tempCalendar.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR))
                            {
                                activity = true;
                                break;
                            }//end if
                        }//end for(a)
                        //highlight
                        row = tab(baseTab + 1) + "<tr class=\"selected\">\n";
                        if(activity)
                            td += tab(baseTab + 2) + "<td class=\"activity selected\">" + current + "</td>\n";
                        else
                            td += tab(baseTab + 2) + "<td class=\"selected\">" + current + "</td>\n";
                    }else{
                        //initialise plain cell with date link
                        String tempTd = tab(baseTab + 2) + "<td>"
                                + "<a href=\"" + link + "?time=" + now.getTimeInMillis() + "\">" + current + "</a></td>\n";
                        for(Appointment a :appointments)
                        {
                            //if there is any activity on this day, bold the link
                            Calendar tempCalendar = Calendar.getInstance(Locale.US);
                            tempCalendar.setTime(a.getTime());

                            if(tempCalendar.get(Calendar.YEAR) == now.get(Calendar.YEAR)
                                    && tempCalendar.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR))
                            {
                                tempTd = tab(baseTab + 2) + "<td class=\"activity\">"
                                        + "<a href=\"" + link + "?time=" + now.getTimeInMillis() + "\">" + current + "</a></td>\n";
                                break;
                            }//end if
                        }//end for(a)
                        td += tempTd;
                    }//end if
                }//end if date is out of range
            }//end for(j)
            str += row;
            str += td;
            str += tab(baseTab + 1) + "</tr>\n";
        }//end for(i)
        return str + tab(baseTab) + "</table>";
    }

    /**
     * builds the table for weekly schedule css classes include:
     * single_approved/single_pending start_approved/start_pending
     * end_approved/end_pending overlapping appointments are marked with '*' all
     * appointments will link to Details with parameter
     * appointment=appointmentID respectively
     * <p/>
     * @param appointments all the appointments the current user is involved in
     * @param selected the selected date in miliseconds
     * @throws SQLException if there was a problem with a databse query
     * @throws ParseException if there was a problem converting the list of
     * usernames to uer ids
     * @throws ClassNotFoundException if there was a problem with the database
     * driver
     * @throws InstantiationException if there was a problem with the database
     * driver
     * @throws IllegalAccessException if there was a problem with the database
     * driver
     */
    public static String buildWeek(LinkedList<Appointment> appointments, long selected) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        int baseTab = 7;
        SimpleDateFormat tableHeader = new SimpleDateFormat("EEE dd/MM");
        //setup calendar
        Calendar now = Calendar.getInstance(Locale.US);
        now.setTimeInMillis(selected);
        now.set(Calendar.MILLISECOND, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.HOUR_OF_DAY, 6);//start from 6am
        int selectedDate = now.get(Calendar.DAY_OF_WEEK);
        //make it sunday
        now.set(Calendar.DAY_OF_WEEK, 1);

        //build array of appointments
        Appointment[][] app = new Appointment[14][7];
        int[][] formatting = new int[14][7];

        for(int i = 0; i < 14; i++)
        {
            for(int j = 0; j < 7; j++)
            {
                for(Appointment a :appointments)
                {
                    if(a.getTime().equals(now.getTime()))
                    {
                        //check for overlap at each hour
                        if(app[i][j] != null)
                        {
                            //prevent duplicates
                            if(!app[i][j].getOverlap().contains(a))
                            {
                                //add to existing overlapped list
                                app[i][j].getOverlap().add(a);
                                //point current appointments overlap to 
                                //existing appointments overlap list
                                a.setOverlap(app[i][j].getOverlap());
                            }
                        }
                        DatabaseController dbm = new DatabaseController();
                        if(!dbm.isMeeting(a.getAppointmentId()))
                        {
                            //user blocked timeslot
                            for(int k = 0; k < a.getDuration(); k++)
                            {
                                app[i + k][j] = a;
                                formatting[i + k][j] = BLOCKED;
                            }//end for(k)
                        }else{
                            //meeting
                            if(a.getDuration() > 1)
                            {
                                app[i][j] = a;
                                formatting[i][j] = BEGINNING;
                                for(int k = 1; k < a.getDuration(); k++)
                                {
                                    app[i + k][j] = a;
                                    formatting[i + k][j] = k;
                                }//end for(k)
                                formatting[i + a.getDuration() - 1][j] = END;
                            }else
                            {
                                app[i][j] = a;
                                formatting[i][j] = SINGLE;
                            }
                        }//end if(meeting/blocked)
                    }//end if
                }//end for (a)
                now.add(Calendar.DAY_OF_WEEK, 1);
            }//end for(j)
            
            //go back to sunday
            now.add(Calendar.DAY_OF_WEEK, -7);
            //increment hour
            now.add(Calendar.HOUR_OF_DAY, 1);
        }//end for(i)

        //build table
        Tr[] table = new Tr[15];
        //create header
        Cell[] row = new Cell[8];
        row[0] = new Cell();
        row[0].setFormat("none");
        for(int i = 1; i <= 7; i++)
        {
            now.set(Calendar.DAY_OF_WEEK, i);
            if(i == selectedDate)
                row[i] = new Cell("selected", "&nbsp;" + tableHeader.format(now.getTime()) + "&nbsp;", "", "th", tab(baseTab + 2));
            else
                row[i] = new Cell("blank", "&nbsp;" + tableHeader.format(now.getTime()) + "&nbsp;", "", "th", tab(baseTab + 2));
        }//end for(i)
        table[0] = new Tr(row, tab(baseTab + 1), "blank");

        //table body
        for(int i = 0; i < 14; i++)
        {
            row = new Cell[8];
            row[0] = new Cell("time", (i + 6) + ":00", "", "td", tab(baseTab + 2));
            for(int j = 0; j < 7; j++)
            {
                Cell cel = new Cell();
                cel.setTab(tab(baseTab + 2));

                Appointment a = app[i][j];
                //check if the appointment is approved or not
                DatabaseController dbm = new DatabaseController();
                //start building cells                    
                //if the formatting matrix idetifies there is the beginning of a meeting here
                switch(formatting[i][j])
                {
                    default:
                    {
                        //simmilar to above, we have the end of an appointment block here
                        //is it an approved or pending appointment?
                        //format respectively
                        if(dbm.isApproved(a.getAppointmentId()))
                            cel.setFormat("middle_approved");
                        else
                            cel.setFormat("middle_pending");
                        cel.setLink("Details?appointment=" + a.getAppointmentId());
                        break;
                    }
                    case 0:
                        break;
                    case BEGINNING:
                    case SINGLE:
                    {
                        //check if the appointment has been approved or is pending
                        cel.setLink("Details?appointment=" + a.getAppointmentId());
                        if(dbm.isApproved(a.getAppointmentId()))
                        {
                            //if its approved we dont need to worry about overlap as they can't exist
                            cel.setFormat("start_approved");
                            cel.setText("Meeting<br />Approved");
                        }else
                        {
                            //if the appointment isnt approved
                            //check if its overlapping another non-approved appointment
                            if(a.getOverlap().size() > 1)
                            {
                                //mark it if its overlapped
                                cel.setFormat("start_pending");
                                cel.setText("*Awaiting*<br />Approval");
                            }else
                            {
                                cel.setFormat("start_pending");
                                cel.setText("Awaiting<br />Approval");
                            }
                        }
                        if(formatting[i][j] == SINGLE)
                        {
                            if(dbm.isApproved(a.getAppointmentId()))
                                cel.setFormat("single_approved");
                            else
                                cel.setFormat("single_pending");
                        }
                        break;
                    }
                    case END:
                    {
                        //simmilar to above, we have the end of an appointment block here
                        //is it an approved or pending appointment?
                        //format respectively
                        if(dbm.isApproved(a.getAppointmentId()))
                            cel.setFormat("end_approved");
                        else
                            cel.setFormat("end_pending");
                        cel.setLink("Details?appointment=" + a.getAppointmentId());
                        break;
                    }
                    case BLOCKED:
                        //grey this cell out if its idicated as a blocked time
                        cel.setFormat("blocked");
                }//end switch
                row[j + 1] = cel;
            }//end for(j)
            table[i + 1] = new Tr(row, tab(baseTab + 1), "weekly_schedule");
        }//end for(i)
        return tableToString(table, baseTab);
    }

    /**
     * used to print a table of the possible times, blocked timeslots have css
     * class="blocked" all others have css class="available"
     * <p/>
     * @param params the parameters taken from a form to specify who is in the
     * meeting and what the meeting should all into
     * @param userID the id of the current user
     * @throws SQLException if there was a problem with a databse query
     * @throws ParseException if there was a problem converting the list of
     * usernames to uer ids
     * @throws ClassNotFoundException if there was a problem with the database
     * driver
     * @throws InstantiationException if there was a problem with the database
     * driver
     * @throws IllegalAccessException if there was a problem with the database
     * driver
     */
    public static String possibleTimes(MeetingParams params, int userID) throws SQLException, ParseException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchEntityException
    {
        LinkedList<Integer> users = params.getTo();
        long selected = params.getDate();
        boolean isDateRange = params.isIsDateRange();
        int duration = params.getDuration();
        int baseTab = 2;
        //build a list of all the appointments of all the members
        LinkedList<Appointment> appointments = new LinkedList<Appointment>();
        DatabaseController dbm = new DatabaseController();
        for(Integer i :users)
            appointments.addAll(dbm.getUsersAppointments(i));

        //start building the table
        SimpleDateFormat tableHeader = new SimpleDateFormat("EEE - dd/MM");
        //setup calendar
        Calendar now = Calendar.getInstance(Locale.US);
        now.setTimeInMillis(selected);
        now.set(Calendar.MILLISECOND, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.HOUR_OF_DAY, 6);//start from 6am
        //store selected date for highlighting
        int selectedDate = now.get(Calendar.DAY_OF_WEEK);

        //make it sunday
        now.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        //build array of all appointments
        int[][] formatting = new int[14][7];
        long[][] times = new long[14][7];
        for(int i = 0; i < 14; i++)
        {
            for(int j = 0; j < 7; j++)
            {
                times[i][j] = now.getTimeInMillis();
                if(isDateRange || now.get(Calendar.DAY_OF_WEEK) == selectedDate)
                {
                    for(Appointment a :appointments)
                    {
                        if(a.getTime().equals(now.getTime()))
                        {
                            for(int k = 0; k < a.getDuration(); k++)
                            {
                                //if there is an aprroved appointment or
                                //if the current user has already an approved appointment
                                //then it makes no sense that they would want to suggest here
                                if(dbm.isApproved(a.getAppointmentId()) || dbm.hasAproved(userID, a.getAppointmentId()))
                                    formatting[i + k][j] = BLOCKED;
                                //check if any of the included users have aprroved this appointment
                                for(Integer u :users)
                                    if(dbm.hasAproved(u, a.getAppointmentId()))
                                        formatting[i + k][j] = BLOCKED;
                            }//end for(k)
                        }//end if
                    }//end for(a)
                }else{
                    formatting[i][j] = BLOCKED;
                }//end if(selected date)
                now.add(Calendar.DAY_OF_WEEK, 1);
            }//end for(j)
            //go back to sunday
            now.add(Calendar.DAY_OF_WEEK, -7);
            //increment hour
            now.add(Calendar.HOUR_OF_DAY, 1);
        }//end for(i)
        //block times that are too small for the appointment
        for(int i = 0; i < 14; i++)
        {
            for(int j = 0; j < 7; j++)
            {
                int freespace = 0;
                int k = i;
                //calculate how much free space there is between appointments
                while(k < 14 && formatting[k][j] != BLOCKED)
                {
                    freespace++;
                    k++;
                }//end while(k)
                if(freespace < duration)
                {
                    //block down if there is not enough free time
                    k = i;
                    while(k < 14 && formatting[k][j] != BLOCKED)
                    {
                        formatting[k][j] = BLOCKED;
                        k++;
                    }//end while(k)
                }//end if(not enough freespace)
            }//end for(j)
        }//end for(i)
        //block saturdays and sundays
        for(int i = 0; i < 7; i += 6)
            for(int j = 0; j < 14; j++)
                formatting[j][i] = BLOCKED;

        //block boundary times
        for(int j = 0; j < 7; j++)
            for(int k = 13; k > 14 - duration; k--)
                formatting[k][j] = BLOCKED;

        //build table
        //table header
        Tr[] table = new Tr[15];
        Cell[] row = new Cell[8];
        row[0] = new Cell();
        row[0].setFormat("none");
        for(int i = 1; i <= 7; i++)
        {
            now.set(Calendar.DAY_OF_WEEK, i);
            if(i == selectedDate)
                row[i] = new Cell("selected", "&nbsp;" + tableHeader.format(now.getTime()) + "&nbsp;", "", "th", tab(baseTab + 2));
            else
                row[i] = new Cell("blank", "&nbsp;" + tableHeader.format(now.getTime()) + "&nbsp;", "", "th", tab(baseTab + 2));
        }
        table[0] = new Tr(row, tab(baseTab + 1), "blank");

        //table body
        for(int i = 0; i < 14; i++)
        {
            row = new Cell[8];
            row[0] = new Cell("time", (i + 6) + ":00", "", "td", tab(baseTab + 2));
            for(int j = 0; j < 7; j++)
            {
                Cell cel = new Cell();
                cel.setTab(tab(baseTab + 2));
                if(formatting[i][j] != 0)
                {
                    cel.setFormat("blocked");
                }else{
                    cel.setFormat("available");
                    cel.setLink("FinaliseMeeting?time=" + times[i][j]);
                    cel.setText("select");
                }//end if
                //add the cell to the row
                row[j + 1] = cel;
            }//end for(j)
            //add the row to the table
            table[i + 1] = new Tr(row, tab(baseTab + 1), "weekly_schedule");
        }//end for(i)
        return tableToString(table, baseTab);
    }

    /**
     * used to tab each line the correct amount
     * <p/>
     * @param i input how many tabs are needed
     * @return string containing i amount of "\t"
     */
    private static String tab(int i)
    {
        String tabs = "\t";
        for(int j = 1; j < i; j++)
            tabs += "\t";
        return tabs;
    }

    /**
     *
     * @param table Tr[] representing a weekly schedule
     * @param baseTab integer representing how many tabs in this table needs to
     * be
     * @return String representation of the weekly schedule
     */
    private static String tableToString(Tr[] table, int baseTab)
    {
        String str = "<table class=\"weekly_schedule\">\n";
        for(Tr t :table)
            str += t;
        str += tab(baseTab) + "</table>\n";
        return str;
    }

    //class used to represent a row of a table
    //made up of an array of cell objects
    private static class Tr
    {

        private Cell[] row;
        private String tab;
        private String format;

        public Tr(Cell[] row, String tab, String format)
        {
            this.row = row;
            this.tab = tab;
            this.format = format;
        }

        public Cell[] getRow()
        {
            return row;
        }

        public void setRow(Cell[] row)
        {
            this.row = row;
        }

        public String getTab()
        {
            return tab;
        }

        public void setTab(String tab)
        {
            this.tab = tab;
        }

        public String getFormat()
        {
            return format;
        }

        public void setFormat(String format)
        {
            this.format = format;
        }

        @Override
        public String toString()
        {
            String str = "";
            for(Cell c :row)
                str += c;
            return tab + "<tr class=\"" + format + "\">\n" + str + "</tr>";
        }
    }
    //class used to represent every <td> tag in a table
    //used to ease of formatting and cell manipulation

    private static class Cell
    {

        private String format;
        private String text;
        private String link;
        private String type;
        private String tab;

        public Cell(String format, String text, String link, String type, String tab)
        {
            this.format = format;
            this.text = text;
            this.link = link;
            this.type = type;
            this.tab = tab;
        }
        //the default cell

        public Cell()
        {
            format = "blank";
            text = "&nbsp;";
            link = "";
            type = "td";
            tab = "";
        }

        public String getFormat()
        {
            return format;
        }

        public void setFormat(String format)
        {
            this.format = format;
        }

        public String getText()
        {
            return text;
        }

        public void setText(String text)
        {
            this.text = text;
        }

        public String getLink()
        {
            return link;
        }

        public void setLink(String link)
        {
            this.link = "<a href=\"" + link + "\"><div class=\"link\">";
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getTab()
        {
            return tab;
        }

        public void setTab(String tab)
        {
            this.tab = tab;
        }

        /*
         * default cell will return <td class="blank">&nbsp;</td> anything with
         * the link attribute set will look like: <td class="blank"><a
         * href="somelink"><div class="link:>text</div></a></td> @return string
         * representation of a <td> element
         */
        @Override
        public String toString()
        {
            if(link.equals(""))
                return tab + "<" + type + " class=\"" + format + "\">" + text + "</" + type + ">\n";
            return tab + "<" + type + " class=\"" + format + "\">" + link + text + "</div></a></" + type + ">\n";
        }
    }
}
