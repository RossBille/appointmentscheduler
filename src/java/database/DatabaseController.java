package database;

import com.mysql.jdbc.Connection;
import database.exceptions.NoSuchEntityException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import userInterface.Bean;
import userInterface.MeetingParams;

/**
 * all database connections will come through this class, provides the
 * interfaces to get required information out of the database
 * <p/>
 * @author Ross Bille 3127333
 */
public class DatabaseController
{

    private String dbUrl = "jdbc:mysql://localhost:3306/AppointmentScheduler";
    private Connection conn;
    private String userName = "root";
    private String password = "";

    public DatabaseController() throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
    }
    
    //establish a connection with the database server
    private void getConnection() throws SQLException
    {
        if(conn == null || conn.isClosed())
            conn = (Connection) DriverManager.getConnection(dbUrl, userName, password);
    }
    
    //closes off the current database connection
    private void setDone() throws SQLException
    {
        if(conn != null)
        {
            conn.close();
            conn = null;
        }
    }

    /**
     * queries the database for all the users and creates a list of user objects
     * <p/>
     * @return LinkedList<User> all the users in the system
     */
    public LinkedList<User> getAllUsers() throws SQLException
    {
        getConnection();
        Statement s = conn.createStatement();
        String query = "SELECT * FROM User;";
        ResultSet rs = s.executeQuery(query);

        LinkedList<User> users = new LinkedList<User>();
        //for every result, create a new user object
        while(rs.next())
        {
            User tempUser = new User(
                    rs.getInt("UserID"),
                    rs.getString("Username"),
                    rs.getString("Firstname"),
                    rs.getString("Lastname"),
                    rs.getString("Email"));
            users.add(tempUser);
        }
        rs.close();
        setDone();
        return users;
    }

    /**
     * @return LinkedList<Appointment> all the appointments in the system
     * @throws SQLException
     * @throws ParseException if there was a problem with the date format in the
     * database
     */
    public LinkedList<Appointment> getAllAppointments() throws SQLException, ParseException
    {
        getConnection();
        Statement s = conn.createStatement();
        String query = "SELECT * FROM Appointment;";
        ResultSet rs = s.executeQuery(query);

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LinkedList<Appointment> appointments = new LinkedList<Appointment>();
        while(rs.next())
        {
            Appointment tempAppointment = new Appointment(
                    rs.getInt("AppointmentID"),
                    (Date) formatter.parse(rs.getString("Time")),
                    rs.getInt("duration"),
                    rs.getString("Subject"),
                    rs.getString("Message"));
            appointments.add(tempAppointment);
        }
        rs.close();
        setDone();
        return appointments;
    }

    /**
     * @return LinkedList<AppointmentHistory> List of all the appointmentHistory
     * in the database
     * @throws SQLException
     */
    public LinkedList<AppointmentHistory> getAllAppointmentHistory() throws SQLException
    {
        getConnection();
        Statement s = conn.createStatement();
        String query = "SELECT * FROM AppointmentHistory;";
        ResultSet rs = s.executeQuery(query);

        LinkedList<AppointmentHistory> ah = new LinkedList<AppointmentHistory>();
        while(rs.next())
        {
            AppointmentHistory tempAH = new AppointmentHistory(
                    rs.getInt("UserID"),
                    rs.getInt("AppointmentID"),
                    rs.getBoolean("Approved"),
                    rs.getBoolean("Required"));
            ah.add(tempAH);
        }
        rs.close();
        setDone();
        return ah;
    }

    /**
     * @param id integer representation of a user
     * @return LinkedList<Appointment> A linked list of appointments for a
     * specified user
     * @throws SQLException
     * @throws ParseException
     */
    public LinkedList<Appointment> getUsersAppointments(int id) throws SQLException, ParseException, NoSuchEntityException
    {
        getConnection();
        String query = "SELECT AppointmentID FROM AppointmentHistory WHERE UserID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, id);
        ResultSet rs = s.executeQuery();
        //get all the appointment numbers that this user is associated with
        LinkedList<Integer> appointments = new LinkedList<Integer>();
        while(rs.next())
            appointments.add(rs.getInt("AppointmentID"));
        
        rs.close();
        setDone();
        //fetch the correct appointments
        LinkedList<Appointment> finalList = new LinkedList<Appointment>();
        for(Integer i :appointments)
            finalList.add(getAppointmentById(i));
        return finalList;
    }

    /**
     * @param username the username of a potential user in the database
     * @return User a user from the database represented as a User object
     * @throws NoSuchEntityException if the user doesn't exist in the database
     */
    public User getUserByUsername(String username) throws NoSuchEntityException, SQLException
    {
        getConnection();
        String query = "SELECT * FROM User WHERE Username = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setString(1, username);
        ResultSet rs = s.executeQuery();
        //check if the result is empty
        if(!rs.next())
            throw new NoSuchEntityException(username);
        User u = new User(
                rs.getInt("UserID"),
                rs.getString("Username"),
                rs.getString("Firstname"),
                rs.getString("Lastname"),
                rs.getString("Email"));
        setDone();
        rs.close();
        return u;        
    }
    
     /**
     *
     * @param i the user ID of the user that will be returned @return User a
     * user from the database represented as a User object @throws
     * NoSuchEntityException if the user doesn't exist in the database
     */
    public User getUserByID(int i) throws NoSuchEntityException, SQLException
    {
        getConnection();
        String query = "SELECT * FROM User WHERE userID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, i);
        ResultSet rs = s.executeQuery();
        //check if the result is empty
        if(!rs.next())
            throw new NoSuchEntityException(String.valueOf(i));
        User u = new User(
                rs.getInt("UserID"),
                rs.getString("Username"),
                rs.getString("Firstname"),
                rs.getString("Lastname"),
                rs.getString("Email"));
        setDone();
        rs.close();
        return u;        
    }

    /**
     *
     * @param id
     * @return Appointment the appointment that matches the id
     * @throws SQLException
     * @throws ParseException
     */
    public Appointment getAppointmentById(int id) throws SQLException, ParseException, NoSuchEntityException
    {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        getConnection();
        String query = "SELECT * FROM Appointment WHERE AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, id);
        ResultSet rs = s.executeQuery();
        //check if the result is empty
        if(!rs.next())
            throw new NoSuchEntityException(String.valueOf(id));
        Appointment a = new Appointment(
                rs.getInt("AppointmentID"),
                (Date) formatter.parse(rs.getString("Time")),
                rs.getInt("duration"),
                rs.getString("Subject"),
                rs.getString("Message"));
        setDone();
        rs.close();
        return a;
    }

    /**
     *
     * @param userID
     * @param appointmentID
     * @return true if the user matching userID has approved the appointment
     * matching appointmentID
     * @throws SQLException
     */
    public boolean hasAproved(int userID, int appointmentID) throws SQLException
    {
        getConnection();
        String query = "SELECT Approved FROM AppointmentHistory WHERE UserID = ? AND AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, userID);
        s.setInt(2, appointmentID);
        ResultSet rs = s.executeQuery();
        boolean approved;
        if(rs.next())
            approved = rs.getBoolean("Approved");
        else
            approved = false;
        rs.close();
        setDone();
        return approved;
    }

    /**
     *
     * @param appointmentID
     * @return true if all required users have approved the meeting matching the
     * appointmentID
     * @throws SQLException
     */
    public boolean isApproved(int appointmentID) throws SQLException
    {
        getConnection();
        String query = "SELECT Approved, Required FROM AppointmentHistory WHERE AppointmentID=?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, appointmentID);
        ResultSet rs = s.executeQuery();
        LinkedList<AppointmentHistory> ah = new LinkedList<AppointmentHistory>();
        while(rs.next())
        {
            AppointmentHistory tempAH = new AppointmentHistory(0, 0, rs.getBoolean("Approved"), rs.getBoolean("Required"));
            ah.add(tempAH);
        }
        rs.close();
        setDone();
        //check if all required have been approved
        //if one of the rows isnt approved by a required member, 
        //  the whole meeting is still pending
        for(AppointmentHistory a :ah)
            if(a.isRequired() && !a.isApproved())
                return false;

        return true;
    }

    /**
     *
     * @param appointmentID
     * @return true if there is >1 user for the specified appointment
     * @throws SQLException
     */
    public boolean isMeeting(int appointmentID) throws SQLException
    {
        getConnection();
        String query = "SELECT COUNT(AppointmentID) FROM AppointmentHistory WHERE AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, appointmentID);
        ResultSet rs = s.executeQuery();
        rs.next();
        int i = rs.getInt(1);
        setDone();
        rs.close();
        if(i > 1)
            return true;
        return false;
    }

    /**
     * Add a new meeting to the database with the given parameters
     * <p/>
     * @param time the Date of the appointment to be added
     * @param params an instance of MeetingParams containing all the data needed
     * to create a meeting
     * @param bean an instance of the sessions bean to take userID from etc
     * @return int the appointment id used for the new appointment
     * @throws SQLException
     * @throws ParseException
     */
    public int createMeeting(Date time, MeetingParams params, Bean bean) throws SQLException, ParseException, NoSuchEntityException
    {
        //insert appointment
        int appointmentID = insertAppointment(time, params.getDuration(), params.getSubject(), params.getDescription());
        //is the timeslot a meeting or a blocked slot
        //if there is only 1 user in the appointment
        //then this is actually a blocked timeslot for that user
        //so auto approve it
        LinkedList<Integer> requiredUsers = params.getTo();
        boolean isBlock = requiredUsers.size() <= 1;
        //insert a new row into AppointmentHistory for each required user
        for(Integer i :requiredUsers)
            insertAppointmentHistory(appointmentID, i, isBlock, true);
        //insert a new row for all optional users
        LinkedList<Integer> optionalUsers = params.getOptional();
        for(Integer i :optionalUsers)
            if(!requiredUsers.contains(i))
                insertAppointmentHistory(appointmentID, i, false, false);

        //auto approve for the creating user
        approve(appointmentID, bean.getUserID());        
        return appointmentID;
    }

    /**
     *
     * @param time
     * @param duration
     * @param subject
     * @param message
     * @return int the appointment id used for the new appointment
     * @throws SQLException
     * @throws ParseException
     */
    public int insertAppointment(Date time, int duration, String subject, String message) throws SQLException, ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        getConnection();
        //               INSERT INTO Appointment VALUES(0,time,duration,subject,message);
        String update = "INSERT INTO Appointment VALUES(0,?,?,?,?)";
        java.sql.PreparedStatement s = conn.prepareStatement(update);
        s.setString(1, formatter.format(time));
        s.setInt(2, duration);
        s.setString(3, subject);
        s.setString(4, message);
        s.executeUpdate();
        setDone();
        return getAllAppointments().getLast().getAppointmentId();
    }

    /**
     *
     * @param appointmentID
     * @param userID
     * @param approved
     * @param required
     * @throws SQLException
     */
    public void insertAppointmentHistory(int appointmentID, int userID, boolean approved, boolean required) throws SQLException
    {
        getConnection();
        //               INSERT INTO AppointmentHistory VALUES(userID,appointmentID,approved,required);
        String update = "INSERT INTO AppointmentHistory VALUES (?,?,?,?)";
        java.sql.PreparedStatement s = conn.prepareStatement(update);
        s.setInt(1, userID);
        s.setInt(2, appointmentID);
        s.setBoolean(3, approved);
        s.setBoolean(4, required);
        s.executeUpdate();
        setDone();
    }

    public LinkedList<User> allInvolvedUsers(int appointmentID, boolean required) throws SQLException, NoSuchEntityException
    {
        getConnection();
        String query = "SELECT UserID FROM AppointmentHistory WHERE AppointmentID = ? AND Required = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, appointmentID);
        s.setBoolean(2, required);
        ResultSet rs = s.executeQuery();
        LinkedList<Integer> ids = new LinkedList<Integer>();
        while(rs.next())
            ids.add(rs.getInt("UserID"));
        setDone();
        LinkedList<User> users = new LinkedList<User>();
        for(Integer i :ids)
            users.add(getUserByID(i));
        return users;
    }

    /**
     *
     * @param appointmentID
     * @param userID
     * @throws SQLException
     * @throws
     */
    public void approve(int appointmentID, int userID) throws SQLException, NoSuchEntityException
    {
        try
        {
            //check the user exists
            getUserByID(userID);
        }
        catch(NoSuchEntityException ex)
        {
            throw new NoSuchEntityException("userID:" + userID);
        }
        getConnection();
        String query = "UPDATE AppointmentHistory "
                + "SET Approved=true "
                + "WHERE AppointmentID = ? AND UserID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, appointmentID);
        s.setInt(2, userID);
        int executeUpdate = s.executeUpdate();
        setDone();
        if(executeUpdate < 1)
            throw new NoSuchEntityException("appointmentID:" + appointmentID);
    }

    /**
     * deletes all the corresponding appointmentHistory and the corresponding
     * appointment from the database
     * <p/>
     * @param appointmentID
     * @throws SQLException
     */
    public void deleteMeeting(int appointmentID) throws SQLException
    {
        deleteAppointmentHistory(appointmentID);
        deleteAppointment(appointmentID);
    }
    //delete from the AppointmentHistory table
    private void deleteAppointmentHistory(int appointmentID) throws SQLException
    {
        getConnection();
        String delete = "DELETE FROM AppointmentHistory WHERE AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(delete);
        s.setInt(1, appointmentID);
        s.executeUpdate();
        setDone();
    }
    //delete from the Appointment table
    private void deleteAppointment(int appointmentID) throws SQLException
    {
        getConnection();
        String delete = "DELETE FROM Appointment WHERE AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(delete);
        s.setInt(1, appointmentID);
        s.executeUpdate();
        setDone();
    }

    /**
     * convert string of users to list of userIds
     * <p/>
     * @param usersString
     * @return LinkedList<Integer>
     * @throws NoSuchEntityException
     */
    public LinkedList<Integer> convert(String usersString) throws NoSuchEntityException, SQLException
    {
        String[] users = usersString.split(";");//split the string
        LinkedList<Integer> userIds = new LinkedList<Integer>();
        for(int i = 0; i < users.length; i++)
            userIds.add(getUserByUsername(users[i]).getUserID());
        return userIds;
    }

    /**
     *
     * @param userID
     * @param appointmentID
     * @return true if the user is a required user, false otherwise
     * @throws SQLException
     */
    public boolean isRequired(int userID, int appointmentID) throws SQLException
    {
        getConnection();
        String query = "SELECT Required FROM AppointmentHistory WHERE UserID = ? AND AppointmentID = ?";
        java.sql.PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1, userID);
        s.setInt(2, appointmentID);
        ResultSet rs = s.executeQuery();
        rs.next();
        boolean required = rs.getBoolean("Required");
        setDone();
        rs.close();
        return required;
    }
}
