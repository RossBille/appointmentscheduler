package database;

/**
 * Simple bean class to store information taken from the User table
 * <p/>
 * @author Ross Bille 3127333
 */
public class User implements java.io.Serializable
{

    private int userID;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;

    @Override
    public String toString()
    {
        return firstName + " " + lastName;
    }

    public User(int userID, String userName, String firstName, String lastName, String email)
    {
        this.userID = userID;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 37 * hash + this.userID;
        hash = 37 * hash + (this.userName != null ? this.userName.hashCode() : 0);
        hash = 37 * hash + (this.firstName != null ? this.firstName.hashCode() : 0);
        hash = 37 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        hash = 37 * hash + (this.email != null ? this.email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        final User other = (User) obj;
        if(this.userID != other.userID)
            return false;
        if((this.userName == null) ? (other.userName != null) : !this.userName.equals(other.userName))
            return false;
        if((this.firstName == null) ? (other.firstName != null) : !this.firstName.equals(other.firstName))
            return false;
        if((this.lastName == null) ? (other.lastName != null) : !this.lastName.equals(other.lastName))
            return false;
        if((this.email == null) ? (other.email != null) : !this.email.equals(other.email))
            return false;
        return true;
    }
}