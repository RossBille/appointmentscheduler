package database;

/**
 * Simple bean class to store information taken from the AppointmentHistory
 * table
 * <p/>
 * @author Ross Bille 3127333
 */
public class AppointmentHistory implements java.io.Serializable
{

    private int userId;
    private int appointmentId;
    private boolean approved;
    private boolean required;

    public AppointmentHistory(int userId, int appointmentId, boolean approved, boolean required)
    {
        this.userId = userId;
        this.appointmentId = appointmentId;
        this.approved = approved;
        this.required = required;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public int getAppointmentId()
    {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId)
    {
        this.appointmentId = appointmentId;
    }

    public boolean isApproved()
    {
        return approved;
    }

    public void setApproved(boolean approved)
    {
        this.approved = approved;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 17 * hash + this.userId;
        hash = 17 * hash + this.appointmentId;
        hash = 17 * hash + (this.approved ? 1 : 0);
        hash = 17 * hash + (this.required ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        final AppointmentHistory other = (AppointmentHistory) obj;
        if(this.userId != other.userId)
            return false;
        if(this.appointmentId != other.appointmentId)
            return false;
        if(this.approved != other.approved)
            return false;
        if(this.required != other.required)
            return false;
        return true;
    }
}