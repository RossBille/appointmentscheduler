package database.exceptions;

/**
 * Exception to be thrown when an entity is queried but not found
 * @author Ross Bille 3127333
 */
public class NoSuchEntityException extends Exception 
{
    public NoSuchEntityException(String username) 
    {
        super(String.format("Sorry, %s, does not exist", username));
    }
}
