package database;

import database.exceptions.NoSuchEntityException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Simple bean class used when appointment details are printed. A combination of
 * Appointment and AppointmentHistory
 * <p/>
 * @author rossbille
 */
public class AppointmentDetails implements java.io.Serializable
{

    private Appointment appointment;
    private List<User> requiredUsers, optionalUsers;
    private boolean approved, attending, required;
    private String date;
    private String time, endTime;

    public AppointmentDetails(Appointment appointment, int userID) throws ClassNotFoundException, InstantiationException, InstantiationException, IllegalAccessException, SQLException, NoSuchEntityException
    {
        this.appointment = appointment;
        DatabaseController dbm = new DatabaseController();
        requiredUsers = dbm.allInvolvedUsers(appointment.getAppointmentId(), true);
        optionalUsers = dbm.allInvolvedUsers(appointment.getAppointmentId(), false);
        //check if the current user has already approved this meeting
        attending = dbm.hasAproved(userID, appointment.getAppointmentId());
        //check if the meeting as a whole has been approved (i.e. all required have approved)
        approved = dbm.isApproved(appointment.getAppointmentId());
        required = dbm.isRequired(userID, appointment.getAppointmentId());
        date = new SimpleDateFormat("dd MMMM yyyy").format(appointment.getTime());
        time = new SimpleDateFormat("HH:mm").format(appointment.getTime());
        //calculate the end time
        Calendar end = Calendar.getInstance(Locale.US);
        end.setTime(appointment.getTime());
        end.add(Calendar.HOUR, appointment.getDuration());
        endTime = new SimpleDateFormat("HH:mm").format(end.getTime());
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }

    public Appointment getAppointment()
    {
        return appointment;
    }

    public void setAppointment(Appointment appointment)
    {
        this.appointment = appointment;
    }

    public List<User> getRequiredUsers()
    {
        return requiredUsers;
    }

    public void setRequiredUsers(List<User> requiredUsers)
    {
        this.requiredUsers = requiredUsers;
    }

    public List<User> getOptionalUsers()
    {
        return optionalUsers;
    }

    public void setOptionalUsers(List<User> optionalUsers)
    {
        this.optionalUsers = optionalUsers;
    }

    public boolean isApproved()
    {
        return approved;
    }

    public void setApproved(boolean approved)
    {
        this.approved = approved;
    }

    public boolean isAttending()
    {
        return attending;
    }

    public void setAttending(boolean attending)
    {
        this.attending = attending;
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 97 * hash + (this.appointment != null ? this.appointment.hashCode() : 0);
        hash = 97 * hash + (this.requiredUsers != null ? this.requiredUsers.hashCode() : 0);
        hash = 97 * hash + (this.optionalUsers != null ? this.optionalUsers.hashCode() : 0);
        hash = 97 * hash + (this.approved ? 1 : 0);
        hash = 97 * hash + (this.attending ? 1 : 0);
        hash = 97 * hash + (this.required ? 1 : 0);
        hash = 97 * hash + (this.date != null ? this.date.hashCode() : 0);
        hash = 97 * hash + (this.time != null ? this.time.hashCode() : 0);
        hash = 97 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        final AppointmentDetails other = (AppointmentDetails) obj;
        if(this.appointment != other.appointment && (this.appointment == null || !this.appointment.equals(other.appointment)))
            return false;
        if(this.requiredUsers != other.requiredUsers && (this.requiredUsers == null || !this.requiredUsers.equals(other.requiredUsers)))
            return false;
        if(this.optionalUsers != other.optionalUsers && (this.optionalUsers == null || !this.optionalUsers.equals(other.optionalUsers)))
            return false;
        if(this.approved != other.approved)
            return false;
        if(this.attending != other.attending)
            return false;
        if(this.required != other.required)
            return false;
        if((this.date == null) ? (other.date != null) : !this.date.equals(other.date))
            return false;
        if((this.time == null) ? (other.time != null) : !this.time.equals(other.time))
            return false;
        if((this.endTime == null) ? (other.endTime != null) : !this.endTime.equals(other.endTime))
            return false;
        return true;
    }
}
