package database;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

/**
 * Simple bean class to store information taken from the Appointment table
 * <p/>
 * @author Ross Bille 3127333
 */
public class Appointment implements java.io.Serializable
{

    private int appointmentId;
    private Date time;
    private int duration;
    private String subject;
    private String message;
    // overlap is a list to hold all appointments that overlap with this appointment
    // this is not taken from the database, it is computed when a calendar is generated
    private LinkedList<Appointment> overlap;

    @Override
    public String toString()
    {
        return "Appointment{" + "appointmentId=" + appointmentId + ", time=" + time + ", duration=" + duration + ", subject=" + subject + ", message=" + message + '}';
    }

    public Appointment(int appointmentId, Date time, int duration, String subject, String message)
    {
        this.appointmentId = appointmentId;
        this.time = time;
        this.duration = duration;
        this.subject = subject;
        this.message = message;
        this.overlap = new LinkedList<Appointment>();
        overlap.add(this);
    }

    public LinkedList<Appointment> getOverlap()
    {
        return overlap;
    }

    public void setOverlap(LinkedList<Appointment> overlap)
    {
        this.overlap = overlap;
    }

    public int getAppointmentId()
    {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId)
    {
        this.appointmentId = appointmentId;
    }

    public Date getTime()
    {
        return time;
    }

    public void setTime(Date time)
    {
        this.time = time;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    /**
     * Check if the passed appointment conflicts with this appointment
     * <p/>
     * @param a
     * @return true if the appointments overlap, false otherwise
     */
    public boolean isDirectlyEffectedBy(Appointment a)
    {
        //calculate end time for this appointment
        Calendar thisEnd = Calendar.getInstance(Locale.US);
        thisEnd.setTime(time);
        thisEnd.add(Calendar.HOUR_OF_DAY, this.duration);
        //calculate end time for the comparing appointment
        Calendar aEnd = Calendar.getInstance(Locale.US);
        aEnd.setTime(a.getTime());
        aEnd.add(Calendar.HOUR_OF_DAY, a.getDuration());

        //check for conflict
        return (a.getTime().before(this.time) && aEnd.getTime().after(this.time))
                || (this.time.before(a.getTime()) && thisEnd.getTime().after(a.getTime()))
                || (this.time.equals(a.getTime()) && this.duration == a.getDuration())
                || (a.getTime().equals(this.time));
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 89 * hash + this.appointmentId;
        hash = 89 * hash + (this.time != null ? this.time.hashCode() : 0);
        hash = 89 * hash + this.duration;
        hash = 89 * hash + (this.subject != null ? this.subject.hashCode() : 0);
        hash = 89 * hash + (this.message != null ? this.message.hashCode() : 0);
        hash = 89 * hash + (this.overlap != null ? this.overlap.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        final Appointment other = (Appointment) obj;
        if(this.appointmentId != other.appointmentId)
            return false;
        if(this.time != other.time && (this.time == null || !this.time.equals(other.time)))
            return false;
        if(this.duration != other.duration)
            return false;
        if((this.subject == null) ? (other.subject != null) : !this.subject.equals(other.subject))
            return false;
        if((this.message == null) ? (other.message != null) : !this.message.equals(other.message))
            return false;
        if(this.overlap != other.overlap && (this.overlap == null || !this.overlap.equals(other.overlap)))
            return false;
        return true;
    }
    
}