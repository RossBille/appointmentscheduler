package servlet;

import database.DatabaseController;
import database.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;

/**
 * Servlet class used to prepare the create meeting form
 * <p/>
 * @author rossbille
 */
public class PrepareForm extends HttpServlet
{
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        boolean error = false;
        String errors = new String(); // used to store any errors found serverside
        LinkedList<User> users = null;
        try
        {
            users = new DatabaseController().getAllUsers();
        }
        catch(ClassNotFoundException e)
        {
            error = true;
            errors += "classNotFound";
        }
        catch(IllegalAccessException e)
        {
            error = true;
            errors += "illegalAccess";
        }
        catch(InstantiationException e)
        {
            error = true;
            errors += "instantiation";
        }
        catch(SQLException e)
        {
            error = true;
            errors += "sql";
        }
        if(!error)
        {
            Bean bean = (Bean) request.getSession().getAttribute("bean");
            LinkedList<String> userNames = new LinkedList<String>();
            for(User u :users)
                if(u.getUserID() != bean.getUserID())
                    userNames.add(u.getUserName());
            bean.setUsers(userNames);
            response.sendRedirect("createMeeting.jsp?type=" + request.getParameter("type"));
        }else{
            response.sendRedirect(String.format("Error?errors=%s&page=homePage", errors));
        }//end if
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
