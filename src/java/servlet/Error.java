package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet class for error handling
 * <p/>
 * @author rossbille
 */
public class Error extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String[] errors = request.getParameter("errors").split(",");//split the string
        String message;
        boolean databaseError = false;
        // check if a database connection error has occurred
        for(String s :errors)
        {
            if(s.equals("classNotFound") || s.equals("illegalAccess") || s.equals("instantiation"))
            {
                databaseError = true;
                break;
            }
        }
        //compile error message and appropriate actions
        if(databaseError || request.getParameter("page").equals("details"))
        {
            message = "There was an error accessing the database.<br />\n\t\tIt is most likely that someone has already canceled that meeting";
            message += "<br />\n\t\tPlease <a href=\"Init?refresh=true\">return home</a> and check.";
            message += "<br />\n\t\tIf the problem persists,Please <a href=\"Logout\">log out</a> and log back in to try again.";
        }else if(request.getParameter("page").equals("chooseTime") || request.getParameter("page").equals("homePage"))
        {
            message = "There was an error accessing the database.";
            message += "<br />\n\t\tPlease <a href=\"Logout\">log out</a> and log back in to try again.";
        }else if(request.getParameter("page").equals("meeting"))
        {
            message = "The following inputs were invalid: " + request.getParameter("errors");
            message += "<br />\n\t\tPlease <a href=\"PrepareForm?type=meeting\">return</a> and fix your input.";
        }else if(request.getParameter("page").equals("block"))
        {
            message = "The following inputs were invalid: " + request.getParameter("errors");
            message += "<br />\n\t\tPlease <a href=\"createMeeting.jsp?type=block\">return</a> and fix your input.";
        }else
        {
            message = "There was an unexpeted error";
            message += "<br />\n\t\tPlease <a href=\"Logout\">log out</a> and log back in to try again.";
        }
        //print page
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("\t<head>");
        out.println("\t\t<title>Error!</title>");
        out.println("\t</head>");
        out.println("\t<body>");
        out.println("\t\t<h1>Error!</h1>");
        out.println("\t\t<div>" + message + "</div>");
        out.println("\t</body>");
        out.println("</html>");
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
