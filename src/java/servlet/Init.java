package servlet;

import database.Appointment;
import database.DatabaseController;
import database.User;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;

/**
 * Servlet class used to set up all parameters and beans for the session
 * <p/>
 * @author Ross Bille 3127333
 */ 
public class Init extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods. This is the entry point to the site Sets up
     * the bean for use and directs to the homepage
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            //get instance of bean
            Bean bean = (Bean) request.getSession().getAttribute("bean");
            //if null then create a bean
            //or if the refresh button has been clicked
            if(bean == null || request.getParameter("refresh") != null)
            {
                //create bean
                bean = new Bean();
                //find user
                DatabaseController dbm = new DatabaseController();
                User u = dbm.getUserByUsername(request.getRemoteUser());
                int userId = u.getUserID();
                //find all appointments for user
                LinkedList<Appointment> appointments = dbm.getUsersAppointments(userId);
                //add data to bean
                bean.setUser(u);
                bean.setUserID(userId);
                bean.setAppointments(appointments);
                //put bean into the session
                request.getSession().setAttribute("bean", bean);
            }//end if
            if(request.getParameter("reset") != null)
            {
                //go back to current date
                Calendar today = Calendar.getInstance(Locale.US);
                bean.setSelected(today.getTimeInMillis());
                bean.setOffset(0);
            }else if(request.getParameter("time") != null)
            {
                //date has been changed
                try
                {
                    long time = Long.parseLong(request.getParameter("time"));
                    bean.setSelected(time);
                }
                catch(NumberFormatException e)
                {
                    //reset to today
                    //go back to current date
                    Calendar today = Calendar.getInstance(Locale.US);
                    bean.setSelected(today.getTimeInMillis());
                    bean.setOffset(0);
                }
            }else if(request.getParameter("next") != null)
            {
                long time = bean.getSelected();
                Calendar cal = Calendar.getInstance(Locale.US);
                cal.setTimeInMillis(time);
                cal.add(Calendar.MONTH, 3);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                time = cal.getTimeInMillis();
                bean.setSelected(time);
            }else if(request.getParameter("prev") != null)
            {
                long time = bean.getSelected();
                Calendar cal = Calendar.getInstance(Locale.US);
                cal.setTimeInMillis(time);
                cal.add(Calendar.MONTH, -3);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                time = cal.getTimeInMillis();
                bean.setSelected(time);
            }//end if
            bean.setReload(false);
            response.sendRedirect("homePage.jsp");
        }
        catch(Exception e)
        {
            response.sendRedirect("Error?errors=database&page=homePage");
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
