package servlet;

import database.Appointment;
import database.DatabaseController;
import database.exceptions.NoSuchEntityException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;
import userInterface.MeetingParams;

/**
 * Servlet class to help in the creation of a meeting
 * <p/>
 * @author Ross Bille 3127333
 */
public class FinaliseMeeting extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods. takes meeting parameters and processes them to
     * create a meeting once complete redirects to the Init servlet
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String errors = new String(); // used to store any errors found serverside
        boolean error = false;
        try
        {
            DatabaseController dbm = new DatabaseController();
            MeetingParams params = (MeetingParams) request.getSession().getAttribute("params");
            Bean bean = (Bean) request.getSession().getAttribute("bean");
            long time = Long.parseLong(request.getParameter("time"));
            Calendar date = Calendar.getInstance(Locale.US);
            date.setTimeInMillis(time);
            int createdMeeting = dbm.createMeeting(date.getTime(), params, bean);
            //get that meeting for comparison
            Appointment app = dbm.getAppointmentById(createdMeeting);

            LinkedList<Appointment> appointments = dbm.getUsersAppointments(bean.getUserID());
        }
        catch(ClassNotFoundException e)
        {
            error = true;
            errors += "classNotFound";
        }
        catch(IllegalAccessException e)
        {
            error = true;
            errors += "illegalAccess";
        }
        catch(InstantiationException e)
        {
            error = true;
            errors += "instantiation";
        }
        catch(SQLException e)
        {
            error = true;
            errors += "sql";
        }
        catch(ParseException e)
        {
            error = true;
            errors += "parse";
        }
        catch(NoSuchEntityException e)
        {
            error = true;
            errors += "noSuchEntity";
        }
        if(error)
        {
            response.sendRedirect(String.format("Error?errors=%s&page=chooseTime", errors));
        }else{
            //clear params and bean
            request.getSession().removeAttribute("params");
            request.getSession().removeAttribute("bean");
            //redirect
            response.sendRedirect("Init");
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
