package servlet;

import database.Appointment;
import database.DatabaseController;
import database.exceptions.NoSuchEntityException;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;

/**
 * Servlet class used to process the accept/reject meeting actions .
 * <p/>
 * @author Ross Bille 3127333
 */
public class AcceptMeeting extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods. Takes request parameters to manipulate the
     * database when a meeting is accepted or rejected
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        int appointmentID = 0;
        boolean accept = false;
        String errors = new String(); // used to store any errors found serverside
        boolean error = false;
        Bean bean = (Bean) request.getSession().getAttribute("bean");
        try
        {
            appointmentID = Integer.parseInt(request.getParameter("appointment"));
        }
        catch(NumberFormatException nfe)
        {
            error = true;
            errors += "AppointmentID,";
        }
        try
        {
            accept = Boolean.parseBoolean(request.getParameter("accept"));
        }
        catch(Exception e)
        {
            error = true;
            errors += "accept,";
        }
        if(!error)
        {
            try
            {
                DatabaseController dc = new DatabaseController();
                // get selected appointment from the current list of appointments
                // this way we dont query the database again
                // and we avoid rejecting meetings that the user has no knowledge of yet
                // i.e. User A is viewing the appointment 1's details
                //      User B creates an appointment (2) with User A which conflicts  appointment 1
                //      Appointment 2 will not be rejected, instead User A can see appointment 2 
                //      on the next page load.
                Appointment app = null;
                for(Appointment a :bean.getAppointments())
                    if(a.getAppointmentId() == appointmentID)
                        app = a;
                if(accept)
                {
                    //approve the appointment
                    dc.approve(appointmentID, bean.getUserID());
                    //reject conflicting appointments
                    if(app.getOverlap().size() > 1)
                        for(Appointment a :app.getOverlap())
                            if(app.isDirectlyEffectedBy(a) && !app.equals(a))
                                dc.deleteMeeting(a.getAppointmentId());
                }else
                {
                    //reject the appointment
                    dc.deleteMeeting(appointmentID);
                }
                //clear bean and redirect
                request.getSession().removeAttribute("bean");
                response.sendRedirect("Init");
            }
            catch(ClassNotFoundException e)
            {
                error = true;
                errors += "classNotFound";
            }
            catch(IllegalAccessException e)
            {
                error = true;
                errors += "illegalAccess";
            }
            catch(InstantiationException e)
            {
                error = true;
                errors += "instantiation";
            }
            catch(SQLException e)
            {
                error = true;
                errors += "sql";
            }
            catch(NoSuchEntityException e)
            {
                error = true;
                errors += "noSuchEntity";
            }
        }//end if
        if(error)
            response.sendRedirect(String.format("Error?errors=%s&page=details", errors));
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
