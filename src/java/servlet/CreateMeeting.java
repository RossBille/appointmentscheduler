/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.DatabaseController;
import database.exceptions.NoSuchEntityException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;
import userInterface.MeetingParams;

/**
 * Servlet class to help in the creation of a meeting
 * <p/>
 * @author Ross Bille 3127333
 */
public class CreateMeeting extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods. bundle up request parameters into a bean for
     * use on next page
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Bean bean = (Bean) request.getSession().getAttribute("bean");
        boolean error = false;//assume no errors
        String errors = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String[] to = request.getParameterValues("to");
        String[] optional = request.getParameterValues("cc");
        String subject = request.getParameter("subject");
        String description = request.getParameter("description");
        boolean isDateRange;
        Date date = null;
        int duration = 0;
        //check if week or day was selected
        String weekOrDay = request.getParameter("week_or_day");
        isDateRange = (weekOrDay == null) || (weekOrDay.equals("")) || (weekOrDay.equals("week")) || !(weekOrDay.equals("day"));
        //if the subject is null then this is a blocked timeslot
        String type = "meeting";
        if(subject == null)
        {
            subject = "Blocked Time slot";
            description = "You have blocked this timeslot";
            type = "block";
        }
        try
        {
            date = formatter.parse(request.getParameter("date"));
        }
        catch(ParseException pe)
        {
            error = true;
            errors += "date,";
        }
        try
        {
            duration = Integer.parseInt(request.getParameter("duration"));
        }
        catch(NumberFormatException nfe)
        {
            error = true;
            errors += "duration,";
        }
        if(!error)
        {
            try
            {
                //convert date            
                Calendar time = Calendar.getInstance(Locale.US);
                time.setTime(date);
                //convert user lists
                DatabaseController dbm = new DatabaseController();
                LinkedList<Integer> requiredUsers = new LinkedList<Integer>();
                if(to != null && !to[0].equals(""))
                    for(String s :to)
                        requiredUsers.add(dbm.getUserByUsername(s).getUserID());
                requiredUsers.add(bean.getUserID());

                LinkedList<Integer> optionalUsers = new LinkedList<Integer>();
                if(optional != null && !optional[0].equals(""))
                    for(String s :optional)
                        optionalUsers.add(dbm.getUserByUsername(s).getUserID());
                //set new bean
                MeetingParams params = new MeetingParams(requiredUsers, optionalUsers, subject, description, isDateRange, time.getTimeInMillis(), duration);
                request.getSession().setAttribute("params", params);
                response.sendRedirect("chooseTime.jsp");
            }
            catch(ClassNotFoundException e)
            {
                error = true;
                errors += "classNotFound,";
            }
            catch(IllegalAccessException e)
            {
                error = true;
                errors += "illegalAccess,";
            }
            catch(InstantiationException e)
            {
                error = true;
                errors += "instantiation,";
            }
            catch(NoSuchEntityException e)
            {
                error = true;
                errors += "noSuchUser,";
            }
            catch(SQLException e)
            {
                error = true;
                errors += "sql,";
            }
        }//end if
        if(error)
            response.sendRedirect(String.format("Error?errors=%s&page=%s", errors, type));

    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
