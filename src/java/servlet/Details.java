package servlet;

import database.Appointment;
import database.AppointmentDetails;
import database.DatabaseController;
import database.exceptions.NoSuchEntityException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import userInterface.Bean;

/**
 * Servlet class used to setup the information displayed on the details page
 * <p/>
 * @author rossbille
 */
public class Details extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String errors = new String(); // used to store any errors found serverside
        boolean error = false;
        int appointmentID;
        Bean bean = (Bean) request.getSession().getAttribute("bean");
        int userID = bean.getUserID();
        try
        {
            //convert the appointmentID into an AppointmentDetails object
            appointmentID = Integer.parseInt(request.getParameter("appointment"));
            DatabaseController dbm = new DatabaseController();
            Appointment appointment = null;
            for(Appointment a :bean.getAppointments())
                if(a.getAppointmentId() == appointmentID)
                    appointment = a;
            AppointmentDetails ad = new AppointmentDetails(appointment, userID);
            request.getSession().setAttribute("appointment", ad);
            //convert the overlap into a list of overlapping AppointmentDetails
            LinkedList<Appointment> overlap = (LinkedList<Appointment>) appointment.getOverlap().clone();
            for(Iterator<Appointment> it = overlap.iterator(); it.hasNext();)
            {
                Appointment a = it.next();
                if(!appointment.isDirectlyEffectedBy(a))
                    it.remove();
                else if(appointment.getAppointmentId() == a.getAppointmentId())
                    it.remove();
            }//end for

            LinkedList<AppointmentDetails> overLappingDetails = new LinkedList<AppointmentDetails>();
            for(Appointment a :overlap)
                overLappingDetails.add(new AppointmentDetails(a, bean.getUserID()));
            request.getSession().setAttribute("overlap", overLappingDetails);
            //redirect to details page for display
            response.sendRedirect("details.jsp");
        }
        catch(ClassNotFoundException e)
        {
            error = true;
            errors += "classNotFound";
        }
        catch(IllegalAccessException e)
        {
            error = true;
            errors += "illegalAccess";
        }
        catch(InstantiationException e)
        {
            error = true;
            errors += "instantiation";
        }
        catch(SQLException e)
        {
            error = true;
            errors += "sql";
        }
        catch(NoSuchEntityException e)
        {
            error = true;
            errors += "noSuchUser";
        }
        if(error)
            response.sendRedirect(String.format("Error?errors=%s&page=homePage", errors));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     * <p/>
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * <p/>
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
